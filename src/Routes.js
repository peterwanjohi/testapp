import React from 'react';
import { Router, Scene } from 'react-native-router-flux';
import LoginScreen from './LoginScreen';
import Welcome from './Welcome';
import SMS from'./SMS';
import Contactss from './Contacts';
import Device from './DeviceInfo';
export default class Routes extends React.Component<props>{

    render(){
        return(
            <Router>
                <Scene key='root'>
                    <Scene key = "login" component = {LoginScreen} title="Login"  initial = {true} />
                    <Scene key = "welcome" component = {Welcome}  title = "Welcome" />
                    <Scene key = "sms" component = {SMS}  title = "SMS" />
                    <Scene key = "contacts" component = {Contactss}  title = "Contacts" />
                    <Scene key = "deviceinfo" component = {Device}  title = "Device Info" />
                </Scene>
            </Router>
        );
    }
}