import React,{Component} from 'react';
import { Text, View,StyleSheet,Dimensions} from "react-native";
import DeviceInfo from 'react-native-device-info';
//import RNSimData from 'react-native-sim-data'
export default class Device extends Component{
    constructor () {
        super()
        this.state = {
            DeviceIMEI: '',
            batlevel: ''
        }

    }
    componentDidMount(){
        const IMEI = require('react-native-imei')
        this.setState({
            DeviceIMEI: IMEI.getImei(),
        });
        DeviceInfo.getBatteryLevel().then(batteryLevel => {
            // 0.759999
            this.setState({batlevel: Math.round(batteryLevel*100)});
        });
    }

    render(){

        const width=Dimensions.get('window').width;
        const height = Dimensions.get('window').height;
        var brand = DeviceInfo.getBrand();
        const deviceId = DeviceInfo.getDeviceId();
        const deviceName = DeviceInfo.getDeviceName();
        const deviceCountry = DeviceInfo.getDeviceCountry();
        const carrier = DeviceInfo.getCarrier();
        const buildNumber = DeviceInfo.getBuildNumber();
        const apiLevel = DeviceInfo.getAPILevel();
        const mem =Math.round(DeviceInfo.getMaxMemory()/1048576);
        const locale =DeviceInfo.getDeviceLocale();
        //const phone =RNSimData.getTelephoneNumber();
        const phone = DeviceInfo.getPhoneNumber();
        const serial =DeviceInfo.getSerialNumber();
        const memory =Math.round(DeviceInfo.getTotalMemory()/1048576)
        const tz=DeviceInfo.getTimezone();


        return(
            <View style={styles.container}>
                <Text style={styles.text}>Brand: {brand}</Text>
                <Text style={styles.text}>IMEI: {this.state.DeviceIMEI}</Text>
                <Text style={styles.text}>DeviceId: {deviceId}</Text>
                <Text style={styles.text}>Device Name: {deviceName}</Text>
                <Text>Device Country: {deviceCountry}</Text>
                <Text>Device Carrier {carrier}</Text>
                <Text>Build Number: {buildNumber}</Text>
                <Text>Api Level: {apiLevel}</Text>
                <Text>RAM in Use: {mem} MB</Text>
                <Text>Locale: {locale}</Text>
                <Text>Serial: {serial}</Text>
                <Text>Time Zone: {tz}</Text>
                <Text>Battery Level: {this.state.batlevel}%</Text>
                <Text style={styles.text}>Total RAM: {memory} MB</Text>
                <Text>Phone Number: {phone}</Text>
                <Text style={styles.text}>Screen Size: {width} X {Math.round(height)}</Text>
            </View>

        )
    }

}
const styles=StyleSheet.create({
    container:{
        padding: 20,

    },
    text:{
        fontSize: 16,
        color: '#009'
    }
})