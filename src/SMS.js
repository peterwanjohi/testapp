
import React, { Component } from 'react';

import FCM, { FCMEvent,
    NotificationType,
    WillPresentNotificationResult,
    RemoteNotificationResult } from 'react-native-fcm';
import { Platform } from 'react-native';

import {
    Text,
    View,
    Button,
    Alert
} from 'react-native';

//import firebase from 'react-native-firebase';
//import type { RemoteMessage } from 'react-native-firebase';
//import { PermissionsAndroid } from "react-native";
//import BackgroundJob from 'react-native-background-job';

//import SmsAndroid  from 'react-native-get-sms-android';

/*

firebase.messaging().hasPermission()
  .then(enabled => {
    if (enabled) {
      // user has permissions
      Alert.alert("permissions already granted");
    } else {
      // user doesn't have permission
    }
  });
async function requestReadSmsPermission() {
    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_SMS,
            {
                title: "Auto Verification OTP",
                message: "need access to read sms, to verify OTP"
            }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log("sms read permissions granted", granted);
        } else {
            console.log("sms read permissions denied", denied);
        }
    } catch (err) {
        console.warn(err);
    }
}


*/

const startChatting = function (dispatch) {


    FCM.requestPermissions();
    FCM.getFCMToken()
        .then(token => {
            console.log(token)
        });
    FCM.subscribeToTopic('secret-chatroom');

    FCM.on(FCMEvent.Notification, async (notif) => {
        console.log(notif);

        if (Platform.OS === 'ios') {
            switch (notif._notificationType) {
                case NotificationType.Remote:
                    notif.finish(RemoteNotificationResult.NewData); //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
                    break;
                case NotificationType.NotificationResponse:
                    notif.finish();
                    break;
                case NotificationType.WillPresent:
                    notif.finish(WillPresentNotificationResult.All); //other types available: WillPresentNotificationResult.None
                    break;
            }
        }
    });

    FCM.on(FCMEvent.RefreshToken, token => {
        console.log(token);
    });
}
export default class SMS extends Component {

    //constructor include last message
    constructor(props) {
        super(props);
        this.state = { lastMessage: 0 };
    }

    hello =()=>{
        /* List SMS messages matching the filter */
        var filter = {
            box: 'inbox', // 'inbox' (default), 'sent', 'draft', 'outbox', 'failed', 'queued', and '' for all
            // the next 4 filters should NOT be used together, they are OR-ed so pick one
            read: 1, // 0 for unread SMS, 1 for SMS already read
            /*_id: 1234, // specify the msg id
             address: '+1888------', // sender's phone number
             body: 'How are you', // content to match*/


            // the next 2 filters can be used for pagination
            indexFrom: 0, // start from index 0
            maxCount: 10, // count of SMS to return each time

        };
        startChatting();
        /*SmsAndroid.list({box: 'inbox'}, (fail) => {
                Alert.alert("Failed with this error: " + fail)
            },
            (count, smsList) => {

                var arr = JSON.parse(smsList);

                arr.forEach(function(object){
                    console.log("Object: " + object);
                    console.log("-->" + object.date);
                    console.log("-->" + object.body);
                    this.setState({ lastMessage: object.body })
                })
            });*/
       
    }
    componentDidMount() {
        //requestReadSmsPermission();
        //this.hello();
        this.messageListener = firebase.messaging().onMessage((message: RemoteMessage) => {
            // Process your message as required
        });

    }


    render() {
        return (
            <View>
                <Text> Scheduled jobs: {this.state.lastMessage} </Text>

                <Button title='More' onPress={this.hello}/>
            </View>
        );
    }
}
