
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    TouchableHighlight,
    Image,
    Alert
} from 'react-native';
import { Actions } from 'react-native-router-flux';
export default class LoginView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username   : '',
            password: '',
            isLoggingIn: false,
            message: '',
            access_token: ''
        }
    }
     goToWelcome = () => {
        Actions.welcome()
    }
    onClickListener = (viewId) => {
        Alert.alert("Alert", "Button pressed "+viewId);
    }
    _isLoggingin=()=>{
       //this.Signup();
        this.goToWelcome();

    }



    Signup = async () => {
        let formdata = new FormData();
        formdata.append('username',this.state.username)
        formdata.append('password',this.state.password)
        formdata.append('login','true')
        formdata.append('grant_type','password')
        formdata.append('client_id','i7YlkIu4qdkLZJsnJubhIbeYWP0Qq2NH3D0vatNO')
        formdata.append('client_secret','cx84im8OqngRMM3EftMAfKh1vwEFuSuAD9GH2gE7JxzjE7lJCTI55ZJND8MFPOGkHcFesA9Piy9CgKSzaz3L0bKyspdhq1w8wRouYwhrv3ba8rNwvZ4ppnsebR0rccdB')
        fetch('https://my.jisort.com/registration/login/', {
            method: 'post',
            headers: {
                'accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            },
            body: formdata


        }).then((response) => response.json())
            .then((res) => {
                if(typeof(res.message) !== "undefined"){
                    Alert.alert("Error signing up", "Error: "+ res.message);

                }
                else{
                    this.setState({ access_token: res.access_token });
                    if(typeof (this.state.access_token)!=='undefined'){
                        this.goToWelcome();

                    }
                    else{
                        Alert.alert("Error","Wrong username or password!")
                    }

                }
            }).catch((error) => {
            console.error(error);
        })


    }

    render() {

        return (
            <View style={styles.container}>
                <View style={styles.inputContainer}>
                    <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/message/ultraviolet/50/3498db'}}/>
                    <TextInput style={styles.inputs}
                               placeholder="Username"
                               underlineColorAndroid='transparent'
                               onChangeText={(username) => this.setState({username})}/>
                </View>

                <View style={styles.inputContainer}>
                    <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/key-2/ultraviolet/50/3498db'}}/>
                    <TextInput style={styles.inputs}
                               placeholder="Password"
                               secureTextEntry={true}
                               underlineColorAndroid='transparent'
                               onChangeText={(password) => this.setState({password})}/>
                </View>

                <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} disabled={this.state.isLoggingIn||!this.state.username||!this.state.password} onPress={() => this._isLoggingin()}>
                    <Text style={styles.loginText}>Login</Text>
                </TouchableHighlight>

                <TouchableHighlight style={styles.buttonContainer} onPress={() => this.onClickListener('restore_password')}>
                    <Text>Forgot your password?</Text>
                </TouchableHighlight>

                <TouchableHighlight style={styles.buttonContainer} onPress={() => this.onClickListener('register')}>
                    <Text>Register</Text>
                </TouchableHighlight>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop:50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#DCDCDC',
    },
    inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#FFFFFF',
        borderRadius:30,
        borderBottomWidth: 1,
        width:250,
        height:45,
        marginBottom:20,
        flexDirection: 'row',
        alignItems:'center'
    },
    inputs:{
        height:45,
        marginLeft:16,
        marginTop: 20,
        borderBottomColor: '#FFFFFF',
        flex:1,
    },
    inputIcon:{
        width:30,
        height:30,
        marginLeft:15,

        justifyContent: 'center'
    },
    buttonContainer: {
        height:45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:20,
        width:250,
        borderRadius:30,
    },
    loginButton: {
        backgroundColor: "#00b5ec",
    },
    loginText: {
        color: 'white',
    }
});